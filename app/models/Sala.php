<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 06:08 PM
 */


class Sala extends Eloquent {
    protected $table = 'Sala';
    public $timestamps = false;
    public function carteleras(){
        return $this->hasMany('Cartelera', 'sala_id');
    }
    public function cine(){
        return $this->belongsTo('Cine', 'cine_id');
    }
    public function tipo(){
        return $this->belongsTo('TipoSala', 'tiposala_id');
    }
}