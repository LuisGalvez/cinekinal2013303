<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 07:49 PM
 */

class Prestreno extends Eloquent {
    protected $table = 'Prestrenos';
    public $timestamps = false;
    public function pelicula(){
        return $this->belongsTo('Pelicula', 'pelicula_id');
    }
}