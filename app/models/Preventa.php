<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 07:42 PM
 */

class Preventa extends Eloquent {
    protected $table = 'Preventa';
    public $timestamps = false;
    public function pelicula(){
        return $this->belongsTo('Pelicula', 'pelicula_id');
    }
}