<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 19/07/15
 * Time: 07:54 PM
 */

class Pelicula extends Eloquent {
    protected $table = 'Pelicula';
    public $timestamps = false;
    public function prestrenos(){
        return $this->hasMany('Prestreno', 'pelicula_id');
    }
    public function preventas(){
        return $this->hasMany('Preventas', 'pelicula_id');
    }
    public function carteleras(){
        return $this->hasMany('Cartelera', 'pelicula_id');
    }
}