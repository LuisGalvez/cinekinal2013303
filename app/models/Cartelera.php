<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 06:16 PM
 */

class Cartelera extends Eloquent {
    protected $table = 'Cartelera';
    public $timestamps = false;

    public function sala(){
        return $this->belongsTo('Sala', 'sala_id');
    }
    public function pelicula(){
        return $this->belongsTo('Pelicula', 'pelicula_id');
    }
    public function formato(){
        return $this->belongsTo('FormatoPelicula', 'formatopelicula_id');
    }
}