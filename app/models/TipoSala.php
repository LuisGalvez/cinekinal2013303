<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 06:05 PM
 */

class TipoSala extends Eloquent {
    protected $table = 'TipoSala';
    public $timestamps = false;
    public function salas(){
        return $this->hasMany('Sala', 'tiposala_id');
    }
}