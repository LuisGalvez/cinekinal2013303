<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 24/07/15
 * Time: 05:32 PM
 */

class Cine extends Eloquent {
    protected $table = 'Cine';
    public $timestamps = false;

    public function salas(){
        return $this->hasMany('Sala', 'cine_id');
    }
}