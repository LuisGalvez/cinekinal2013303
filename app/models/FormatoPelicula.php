<?php
/**
 * Created by PhpStorm.
 * User: luisgalvez
 * Date: 26/07/15
 * Time: 05:48 PM
 */

class FormatoPelicula extends Eloquent {
    protected $table = 'FormatoPelicula';
    public $timestamps = false;
    public function carteleras(){
        return $this->hasMany('Cartelera', 'formatopelicula_id');
    }
}