<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('login', function(){
    if (Auth::check()) {
        return Redirect::to('inicio');
    }
    return View::make('login');
});

Route::post('log', 'AuthController@IniciarSesion');

Route::get('registrarse', function(){
    return View::make('registrarse');
});

Route::post('reg', 'AuthController@Registrarse');

Route::group(array('before' => 'auth'), function() {

    Route::group(array('prefix' => 'api'), function () {
        Route::resource('carteleras', 'CartelerasController');
        Route::resource('cines', 'CinesController');
        Route::resource('formatos', 'FormatoPeliculasController');
        Route::resource('peliculas', 'PeliculasController');
        Route::resource('prestrenos', 'PrestrenosController');
        Route::resource('preventas', 'PreventasController');
        Route::resource('salas', 'SalasController');
        Route::resource('tipos', 'TipoSalasController');
    });

    Route::get('inicio', 'AuthController@MostrarInicio');
    Route::get('logout', 'AuthController@SalirDeSesion');
    Route::resource('carteleras', 'CartelerasHTMLController');
    Route::resource('cines', 'CinesHTMLController');
    Route::resource('formatos', 'FormatoPeliculasHTMLController');
    Route::resource('peliculas', 'PeliculasHTMLController');
    Route::resource('prestrenos', 'PrestrenosHTMLController');
    Route::resource('preventas', 'PreventasHTMLController');
    Route::resource('salas', 'SalasHTMLController');
    Route::resource('tipos', 'TipoSalasHTMLController');
});