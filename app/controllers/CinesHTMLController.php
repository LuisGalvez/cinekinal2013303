<?php

class CinesHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Cine::paginate(5);
        return View::make('cines.index')
            ->with('cines', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('cines.crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'nombre'  => 'required',
            'direccion'  => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('cines/crear')
                ->withInput();
        } else {
            $data = new Cine;
            $data->nombre = Input::get('nombre');
            $data->direccion  = Input::get('direccion');
            $data->telefono = Input::get('telefono');
            $data->latitud = Input::get('latitud');
            $data->longitud = Input::get('longitud');
            $data->hora_apertura = Input::get('hora_apertura');
            $data->hora_cierre = Input::get('hora_cierre');
            $data->push();

            return Redirect::to('cines');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Cine::find($id);
        return View::make('cines.mostrar')
            ->with('cine', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Cine::find($id);
        return View::make('cines.editar')
            ->with('cine', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'nombre'  => 'required',
            'direccion'  => 'required',
            'telefono' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'hora_apertura' => 'required',
            'hora_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('cines/'.$id.'/edit')
                ->withInput();
        } else {
            $data = Cine::find($id);
            $data->nombre = Input::get('nombre');
            $data->direccion  = Input::get('direccion');
            $data->telefono = Input::get('telefono');
            $data->latitud = Input::get('latitud');
            $data->longitud = Input::get('longitud');
            $data->hora_apertura = Input::get('hora_apertura');
            $data->hora_cierre = Input::get('hora_cierre');
            $data->push();

            return Redirect::to('cines');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = Cine::find($id);

        $data->delete();
        return Redirect::to('cines');
	}


}
