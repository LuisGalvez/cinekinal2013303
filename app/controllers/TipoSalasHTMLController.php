<?php

class TipoSalasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = TipoSala::paginate(5);
        return View::make('tipos.index')
            ->with('tipos', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('tipos.crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'nombre'  => 'required',
            'descripcion'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('tipos/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new TipoSala();
            $data->nombre = Input::get('nombre');
            $data->descripcion  = Input::get('descripcion');
            $data->push();

            return Redirect::to('tipos');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = TipoSala::find($id);
        return View::make('tipos.mostrar')
            ->with('tipo', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = TipoSala::find($id);
        return View::make('tipos.editar')
            ->with('tipo', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'nombre'  => 'required',
            'descripcion'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('tipos/'.$id.'/edit')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = TipoSala::find($id);
            $data->nombre = Input::get('nombre');
            $data->descripcion  = Input::get('descripcion');
            $data->push();

            return Redirect::to('tipos');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = TipoSala::find($id);

        $data->delete();
        return Redirect::to('tipos');
	}


}
