<?php

class CartelerasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Cartelera::paginate(5);
		return View::make('carteleras.index')
            ->with('carteleras', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $salas = Sala::all();
        $peliculas = Pelicula::all();
        $formatos = FormatoPelicula::all();
        return View::make('carteleras.crear')
            ->with('salas', $salas)
            ->with('peliculas', $peliculas)
            ->with('formatos', $formatos);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'sala'  => 'required',
            'pelicula'  => 'required',
            'formato' => 'required',
            'lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('carteleras/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new Cartelera;
            $data->sala_id = Input::get('sala');
            $data->pelicula_id  = Input::get('pelicula');
            $data->formatopelicula_id = Input::get('formato');
            $data->formato_lenguaje = Input::get('lenguaje');
            $data->fecha = Input::get('fecha');
            $data->hora = Input::get('hora');
            $data->push();

            return Redirect::to('carteleras');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Cartelera::find($id);
        $salas = Sala::all();
        $peliculas = Pelicula::all();
        $formatos = FormatoPelicula::all();
		return View::make('carteleras.mostrar')
            ->with('cartelera', $data)
            ->with('salas', $salas)
            ->with('peliculas', $peliculas)
            ->with('formatos', $formatos);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Cartelera::find($id);
        $salas = Sala::all();
        $peliculas = Pelicula::all();
        $formatos = FormatoPelicula::all();
        return View::make('carteleras.editar')
            ->with('cartelera', $data)
            ->with('salas', $salas)
            ->with('peliculas', $peliculas)
            ->with('formatos', $formatos);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'sala'  => 'required',
            'pelicula'  => 'required',
            'formato' => 'required',
            'lenguaje' => 'required',
            'fecha' => 'required',
            'hora' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('carteleras/'.$id.'/edit')
                ->withInput();
        } else {
            $data = Cartelera::find($id);
            $data->sala_id = Input::get('sala');
            $data->pelicula_id  = Input::get('pelicula');
            $data->formatopelicula_id = Input::get('formato');
            $data->formato_lenguaje = Input::get('lenguaje');
            $data->fecha = Input::get('fecha');
            $data->hora = Input::get('hora');
            $data->push();

            return Redirect::to('carteleras');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Cartelera::find($id);

        $data->delete();
        return Redirect::to('carteleras');
	}


}
