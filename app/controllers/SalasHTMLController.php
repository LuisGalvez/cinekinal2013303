<?php

class SalasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Sala::paginate(5);
        return View::make('salas.index')
            ->with('salas', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

        $cines = Cine::all();
        $tipos = TipoSala::all();
        return View::make('salas.crear')
            ->with('cines', $cines)
            ->with('tipos', $tipos);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'cine'  => 'required',
            'numero'  => 'required',
            'tipo'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('salas/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new Sala;
            $data->cine_id = Input::get('cine');
            $data->numero  = Input::get('numero');
            $data->tiposala_id  = Input::get('tipo');
            $data->push();

            return Redirect::to('salas');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Sala::find($id);
        $cines = Cine::all();
        $tipos = TipoSala::all();
        return View::make('salas.mostrar')
            ->with('sala', $data)
            ->with('cines', $cines)
            ->with('tipos', $tipos);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Sala::find($id);
        $cines = Cine::all();
        $tipos = TipoSala::all();
        return View::make('salas.editar')
            ->with('sala', $data)
            ->with('cines', $cines)
            ->with('tipos', $tipos);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'cine'  => 'required',
            'numero'  => 'required',
            'tipo'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('salas/'.$id.'/edit')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = Sala::find($id);
            $data->cine_id = Input::get('cine');
            $data->numero  = Input::get('numero');
            $data->tiposala_id  = Input::get('tipo');
            $data->push();

            return Redirect::to('salas');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = Sala::find($id);

        $data->delete();
        return Redirect::to('salas');
	}


}
