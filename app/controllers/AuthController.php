<?php

class AuthController extends \BaseController {

    public function IniciarSesion()
    {
        $email = Input::get('email');
        $password = Input::get('password');

        if (Auth::attempt(array('email' => $email, 'password' => $password)))
        {
            return Redirect::intended('inicio');
        }else{
            return Redirect::back()
                ->withInput();
        }

    }

    public function SalirDeSesion()
    {
        Auth::logout();

        return Redirect::to('/');
    }

    public  function Registrarse(){

        $requires = array(
            'username' => 'required|alpha|min:2',
            'email' => 'required|email|unique:Users',
            'password' => 'required|alpha_num|'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('registrarse')
                ->withErrors($validation)
                ->withInput(Input::except('password'));
        } else {
            $data = new User;
            $data->username = Input::get('username');
            $data->email = Input::get('email');
            $data->password = Hash::make(Input::get('password'));
            $data->save();
            return Redirect::to('inicio');
        }
    }

    public function MostrarInicio(){
        return View::make('inicio');
    }

}
