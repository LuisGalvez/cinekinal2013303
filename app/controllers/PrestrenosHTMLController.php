<?php

class PrestrenosHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Prestreno::paginate(5);
        $peliculas = Pelicula::all();
        return View::make('prestrenos.index')
            ->with('prestrenos', $data)
            ->with('peliculas', $peliculas);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $peliculas = Pelicula::all();
        return View::make('prestrenos.crear')
            ->with('peliculas', $peliculas);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'pelicula'  => 'required',
            'fecha_apertura'  => 'required',
            'fecha_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('prestrenos/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new Prestreno();
            $data->pelicula_id = Input::get('pelicula');
            $data->fecha_apertura  = Input::get('fecha_apertura');
            $data->fecha_cierre = Input::get('fecha_cierre');
            $data->push();

            return Redirect::to('prestrenos');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Prestreno::find($id);
        $peliculas = Pelicula::all();
        return View::make('prestrenos.mostrar')
            ->with('prestreno', $data)
            ->with('peliculas', $peliculas);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Prestreno::find($id);
        $peliculas = Pelicula::all();
        return View::make('prestrenos.editar')
            ->with('prestreno', $data)
            ->with('peliculas', $peliculas);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'pelicula'  => 'required',
            'fecha_apertura'  => 'required',
            'fecha_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('prestrenos/'.$id.'/edit')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = Prestreno::find($id);
            $data->pelicula_id = Input::get('pelicula');
            $data->fecha_apertura  = Input::get('fecha_apertura');
            $data->fecha_cierre = Input::get('fecha_cierre');
            $data->push();

            return Redirect::to('prestrenos');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = Prestreno::find($id);

        $data->delete();
        return Redirect::to('prestrenos');
	}


}
