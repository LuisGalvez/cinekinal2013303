<?php

class PeliculasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Pelicula::paginate(5);
        return View::make('peliculas.index')
            ->with('peliculas', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('peliculas.crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'titulo'  => 'required',
            'sinopsis'  => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('peliculas/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new Pelicula;
            $data->titulo = Input::get('titulo');
            $data->sinopsis  = Input::get('sinopsis');
            $data->trailer_url = Input::get('trailer_url');
            $data->image = Input::get('image');
            $data->rated = Input::get('rated');
            $data->genero = Input::get('genero');
            $data->push();

            return Redirect::to('peliculas');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Pelicula::find($id);
        return View::make('peliculas.mostrar')
            ->with('pelicula', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Pelicula::find($id);
        return View::make('peliculas.editar')
            ->with('pelicula', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'titulo'  => 'required',
            'sinopsis'  => 'required',
            'trailer_url' => 'required',
            'image' => 'required',
            'rated' => 'required',
            'genero' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('peliculas/'.$id.'/edit')
                ->withInput();
        } else {
            $data = Pelicula::find($id);
            $data->titulo = Input::get('titulo');
            $data->sinopsis  = Input::get('sinopsis');
            $data->trailer_url = Input::get('trailer_url');
            $data->image = Input::get('image');
            $data->rated = Input::get('rated');
            $data->genero = Input::get('genero');
            $data->push();

            return Redirect::to('peliculas');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = Pelicula::find($id);

        $data->delete();
        return Redirect::to('peliculas');
	}


}
