<?php

class FormatoPeliculasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = FormatoPelicula::paginate(5);
        return View::make('formatos.index')
            ->with('formatos', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('formatos.crear');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'nombre'  => 'required',
            'descripcion'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('formatos/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new FormatoPelicula;
            $data->nombre = Input::get('nombre');
            $data->descripcion  = Input::get('descripcion');
            $data->push();

            return Redirect::to('formatos');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = FormatoPelicula::find($id);
        return View::make('formatos.mostrar')
            ->with('formato', $data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = FormatoPelicula::find($id);
        return View::make('formatos.editar')
            ->with('formato', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'nombre'  => 'required',
            'descripcion'  => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('formatos/'.$id.'/edit')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = FormatoPelicula::find($id);
            $data->nombre = Input::get('nombre');
            $data->descripcion  = Input::get('descripcion');
            $data->push();

            return Redirect::to('formatos');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = FormatoPelicula::find($id);

        $data->delete();
        return Redirect::to('formatos');
	}


}
