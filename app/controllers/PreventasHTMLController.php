<?php

class PreventasHTMLController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Preventa::paginate(5);
        return View::make('preventas.index')
            ->with('preventas', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $peliculas = Pelicula::all();
        return View::make('preventas.crear')
            ->with('peliculas', $peliculas);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $requires = array(
            'pelicula'  => 'required',
            'fecha_apertura'  => 'required',
            'fecha_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('preventas/create')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = new Preventa;
            $data->pelicula_id = Input::get('pelicula');
            $data->fecha_apertura  = Input::get('fecha_apertura');
            $data->fecha_cierre = Input::get('fecha_cierre');
            $data->push();

            return Redirect::to('preventas');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Preventa::find($id);
        $peliculas = Pelicula::all();
        return View::make('preventas.mostrar')
            ->with('preventa', $data)
            ->with('peliculas', $peliculas);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Preventa::find($id);
        $peliculas = Pelicula::all();
        return View::make('preventas.editar')
            ->with('preventa', $data)
            ->with('peliculas', $peliculas);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $requires = array(
            'pelicula'  => 'required',
            'fecha_apertura'  => 'required',
            'fecha_cierre' => 'required'
        );
        $validation = Validator::make(Input::all(), $requires);

        if ($validation->fails()) {
            return Redirect::to('preventas/'.$id.'/edit')
                ->withInput()
                ->withErrors($validation);
        } else {
            $data = Preventa::find($id);
            $data->pelicula_id = Input::get('pelicula');
            $data->fecha_apertura  = Input::get('fecha_apertura');
            $data->fecha_cierre = Input::get('fecha_cierre');
            $data->push();

            return Redirect::to('preventas');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = Preventa::find($id);

        $data->delete();
        return Redirect::to('preventas');
	}


}
